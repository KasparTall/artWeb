package ee.bcs.koolitus.exhibitions.controller;

import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcs.koolitus.exhibitions.dao.Exhibitions;
import ee.bcs.koolitus.exhibitions.dao.MemberExhibition;
import ee.bcs.koolitus.exhibitions.resource.ExhibitionResource;

@Path("/exhibitions")
public class ExhibitionController {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Set<Exhibitions> getAllExhibitions() {
		return ExhibitionResource.getAllExhibitions();
	}

	@GET
	@Path("/{idExhibition}")
	@Produces(MediaType.APPLICATION_JSON)
	public Exhibitions getExhibitionByIdExhibition(@PathParam("idExhibition") int idExhibition) {
		return ExhibitionResource.getExhibitionById(idExhibition);
	}

	@GET
	@Path("/membexhibs/{idMember}")
	@Produces(MediaType.APPLICATION_JSON)
	public Set<Exhibitions> getExhibitionsByMemberId(@PathParam("idMember") int idMember) {
		return ExhibitionResource.getExhibitionsByMemberId(idMember);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Exhibitions addExhibition(Exhibitions exhibition) {
		return ExhibitionResource.addExhibition(exhibition);
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void updateExhibition(Exhibitions exhibition) {
		ExhibitionResource.updateExhibition(exhibition);
	}

	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	public void deleteExhibition(Exhibitions exhibition) {
		ExhibitionResource.deleteExhibition(exhibition);
	}

	@DELETE
	@Path("/members/{memberId}/exhibitions/{exhibitionId}")
	@Consumes(MediaType.APPLICATION_JSON)
	public void deleteExhibitionFromMember(@PathParam("memberId") int memberId,
			@PathParam("exhibitionId") int exhibitionId) {
		ExhibitionResource.deleteExhibitionFromMember(memberId, exhibitionId);
	}

	@POST
	@Path("/members/{memberId}/exhibitions/{exhibitionId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void addExhibitionToMember(@PathParam("memberId") int memberId,
			@PathParam("exhibitionId") int exhibitionId) {
		ExhibitionResource.addExhibitionToMember(new MemberExhibition().setIdExhib(exhibitionId).setIdMemb(memberId));
	}

}
