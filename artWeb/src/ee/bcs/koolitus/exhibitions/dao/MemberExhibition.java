package ee.bcs.koolitus.exhibitions.dao;

public class MemberExhibition {
	private int idMemb;
	private int idExhib;
	
	public int getIdMemb() {
		return idMemb;
	}
	public MemberExhibition setIdMemb(int idMemb) {
		this.idMemb = idMemb;
		return this;
	}
	public int getIdExhib() {
		return idExhib;
	}
	public MemberExhibition setIdExhib(int idExhib) {
		this.idExhib = idExhib;
		return this;
	}

	@Override
	public String toString() {
		return "MemberExhibition[idMemb=" + idMemb + ", idExhib= " + idExhib + "]";
	}
	
}
