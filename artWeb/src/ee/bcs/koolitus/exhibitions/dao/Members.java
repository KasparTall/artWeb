package ee.bcs.koolitus.exhibitions.dao;

import java.util.Date;

public class Members {

	private int idMembers;
	private String firstName;
	private String lastName;
	private Date birthday;
	private String eMail;
	private String password;


	public int getIdMembers() {
		return idMembers;
	}

	public Members setIdMembers(int idMembers) {
		this.idMembers = idMembers;
		return this;
	}

	public String getFirstName() {
		return firstName;
	}

	public Members setFirstName(String firstName) {
		this.firstName = firstName;
		return this;
	}

	public String getLastName() {
		return lastName;
	}

	public Members setLastName(String lastName) {
		this.lastName = lastName;
		return this;
	}

	public Date getBirthday() {
		return birthday;
	}

	public Members setBirthday(Date birthday) {
		this.birthday = birthday;
		return this;
	}

	public String geteMail() {
		return eMail;
	}

	public Members seteMail(String eMail) {
		this.eMail = eMail;
		return this;
	}

	public String getPassword() {
		return password;
	}

	public Members setPassword(String password) {
		this.password = password;
		return this;
	}

	@Override
	public String toString() {
		return "Members[idMembers=" + idMembers + ", firstName= " + firstName + ", lastName=" + lastName + ", birthday="
				+ birthday + ", eMail=" + eMail + ", passowrd=" + password+ "]"; //+ ", exItem="  //+ exItem + "]";
	}
}