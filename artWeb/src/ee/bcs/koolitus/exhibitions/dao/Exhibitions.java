package ee.bcs.koolitus.exhibitions.dao;

import java.util.Date;

public class Exhibitions {
	private int idExhibition;
	private String title;
	private String description;
	private String location;
	private Date eventDate;
	private int ageRestriction;

	public int getIdExhibition() {
		return idExhibition;
	}

	public Exhibitions setIdExhibition(int idExhibition) {
		this.idExhibition = idExhibition;
		return this;
	}

	public String getTitle() {
		return title;
	}

	public Exhibitions setTitle(String title) {
		this.title = title;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public Exhibitions setDescription(String description) {
		this.description = description;
		return this;
	}

	public String getLocation() {
		return location;
	}

	public Exhibitions setLocation(String location) {
		this.location = location;
		return this;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public Exhibitions setEventDate(Date eventDate) {
		this.eventDate = eventDate;
		return this;
	}

	public int getAgeRestriction() {
		return ageRestriction;
	}

	public Exhibitions setAgeRestriction(int ageRestriction) {
		this.ageRestriction = ageRestriction;
		return this;
	}

	@Override
	public String toString() {
		return "Exhibitions[idExhibition=" + idExhibition + ", title= " + title + ", description="
				+ description + ", location=" + location + ", eventDate=" + eventDate +", ageRestriction=" + ageRestriction +"\n"+ "]";
	}
}
