var urli = window.location.href;
var id = urli.split("=")[1];

var sessId = sessionStorage.getItem('sessionId');
var membId = sessionStorage.getItem('membId');

console.log("session id=" + sessId + " exId=" + id);
sessionStorage.setItem('membId', membId);

var member;
function getMemberById() {
	$
			.ajax({
				url : "http://localhost:8080/ArtWeb/rest/members/" + id,
				type : "GET",
				contentType : "application/json; charset=utf-8",
				success : function(member) {
					var tableBody = document
							.getElementById("profileOnMemberPage");
					var tableContent = "";

					tableContent = tableContent
							+ "<tr><td>"
							+ checkIfNullOrUndefined(member.firstName)
							+ "</td><td>"
							+ checkIfNullOrUndefined(member.lastName)
							+ "</td><td>"
							+ checkIfNullOrUndefined(member.birthday.substr(0,
									member.birthday.lastIndexOf("T")))
							+ "</td><td>"
							+ checkIfNullOrUndefined(member.eMail)
							+ "</td><td><button type='button' onClick='deleteMembers("
							+ member.idMembers
							+ ")'>Delete</button> "
							+ "<button type='button' onClick='unhideButton(fillModifyForm("
							+ member.idMembers + "))'>Modify</button>"
							+ "</td></tr>";
					tableBody.innerHTML = tableContent;
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					console.log(textStatus);
				}
			});
}
getMemberById();

function checkIfNullOrUndefined(value) {
	if (value != null && value != undefined && value != "null") {
		return value;
	} else {
		return "";
	}
}

var allMember;
function getAllMember() {
	$.getJSON("http://localhost:8080/ArtWeb/rest/members/", function(member) {
		allMember = member;
		console.log(member);
	});
}
getAllMember();

function fillModifyForm(MembersId) {
	if (id == sessId) {
		for (var i = 0; i < allMember.length; i++) {
			if (allMember[i].idMembers == MembersId) {
				document.getElementById("idMembersModify").value = MembersId;
				document.getElementById("firstNameModify").value = allMember[i].firstName;
				document.getElementById("lastNameModify").value = allMember[i].lastName;
				document.getElementById("birthdayModify").value = allMember[i].birthday
						.substr(0, allMember[i].birthday.lastIndexOf("T"));
				document.getElementById("eMailModify").value = allMember[i].eMail;
				document.getElementById("passwordModify").value = allMember[i].password;
			}
		}
		return "fillModifyForm";
	} else {
		alert("You are not allowed to change other member's profile.");
	}
}

function changeMembers() {

	var modifyMembersJson = {
		"idMembers" : document.getElementById("idMembersModify").value,
		"firstName" : document.getElementById("firstNameModify").value,
		"lastName" : document.getElementById("lastNameModify").value,
		"birthday" : document.getElementById("birthdayModify").value
				+ "T00:00:00+00:00",
		"eMail" : document.getElementById("eMailModify").value,
		"password" : document.getElementById("passwordModify").value,
	}
	console.log(modifyMembersJson);

	var modifyMembers = JSON.stringify(modifyMembersJson);

	$.ajax({
		url : "http://localhost:8080/ArtWeb/rest/members/",
		type : "PUT",
		data : modifyMembers,
		contentType : "application/json; charset=utf-8",
		success : function() {
			getMemberById();
			location.reload();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
		}
	});
}

function deleteMembers(MembersId) {
	if (id == sessId) {
		var deleteMembersJson = {
			"idMembers" : MembersId
		};
		$.ajax({
			url : "http://localhost:8080/ArtWeb/rest/members/",
			type : "DELETE",
			data : JSON.stringify(deleteMembersJson),
			contentType : "application/json; charset=utf-8",
			success : function() {
				window.location.href = 'http://localhost:8080/ArtWeb/';
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				console.log(textStatus);
			}
		});
	} else {
		alert("You are not allowed to delete other member's profile.");
	}
}

var allExhibByMembId;
function getExhibitionsByMemberId() {
	$
			.getJSON(
					"http://localhost:8080/ArtWeb/rest/exhibitions/membexhibs/"
							+ id,
					function(exhibition) {
						allExhibByMembId = exhibition;
						console.log(exhibition);
						var tableBody = document
								.getElementById("exhibitionsOnMemberPage");
						var tableContent = "";
						for (var i = 0; i < exhibition.length; i++) {
							tableContent = tableContent
									+ "<tr><td>"
									+ checkIfNullOrUndefined(exhibition[i].title)
									+ "</td><td>"
									+ checkIfNullOrUndefined(exhibition[i].location)
									+ "</td><td>"
									+ checkIfNullOrUndefined(exhibition[i].eventDate
											.substr(
													0,
													allExhibByMembId[i].eventDate
															.lastIndexOf("T")))
									+ "</td><td>"
									+ "<button type='button' onClick='parent.location=\"http://localhost:8080/ArtWeb/exhibition.html?"
									+ exhibition[i].idExhibition
									+ "\"'>Info</button><button type='button' onClick='deleteExhibitionFromMember("
									+ exhibition[i].idExhibition
									+ ")'>Remove</button>" + "</td></tr>";
						}
						tableBody.innerHTML = tableContent;
					});
}
getExhibitionsByMemberId();

function deleteExhibitionFromMember(exhibitionId) {

	var deleteExhibitionFromMemberJson = {
		"idExhibition" : exhibitionId
	};

	$.ajax({
		url : "http://localhost:8080/ArtWeb/rest/exhibitions/members/" + id
				+ "/exhibitions/" + exhibitionId,
		type : "DELETE",
		data : JSON.stringify(deleteExhibitionFromMemberJson),
		contentType : "application/json; charset=utf-8",
		success : function() {
			location.reload();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
		}
	});
}

function unhideButton(y) {
	var x;
	if (y == 'addExhibitions') {
		x = document.getElementById("tableVisibility");
	} else if (y == "fillModifyForm") {
		x = document.getElementById("modifyMembersForm");
	}
	if (x != undefined) {
		if (x.style.display === "none") {
			x.style.display = "block";
		} else {
			x.style.display = "none";
		}
	}
}

var allExhibitions;
function getAllExhibitions() {
	$.getJSON("http://localhost:8080/ArtWeb/rest/exhibitions/", function(
			exhibition) {
		allExhibitions = exhibition;
		console.log(exhibition);
	});
}
getAllExhibitions();

function getExhibitionsToAddMember() {
	$
			.getJSON(
					"http://localhost:8080/ArtWeb/rest/exhibitions/",
					function(exhibition) {
						allExhibitions = exhibition;
						console.log(exhibition);
						var tableBody = document
								.getElementById("exhibitionsToMember");
						var tableContent = "";

						for (var i = 0; i < exhibition.length; i++) {
							var isExisting = false;
							for (var j = 0; j < allExhibByMembId.length; j++) {
								if (exhibition[i].idExhibition == allExhibByMembId[j].idExhibition) {
									isExisting = true;
								}
							}
							if (!isExisting) {
								tableContent = tableContent
										+ "<tr><td>"
										+ "<input value='"
										+ exhibition[i].idExhibition
										+ "' type='checkbox' id='notAttendingYet'></td><td>"
										+ checkIfNullOrUndefined(exhibition[i].title)
										+ "</td><td>"
										+ checkIfNullOrUndefined(exhibition[i].location)
										+ "</td><td>"
										+ checkIfNullOrUndefined(exhibition[i].eventDate
												.substr(
														0,
														exhibition[i].eventDate
																.lastIndexOf("T")))
										+ "</td><td>"
										+ "<button type='button' onClick='parent.location=\"http://localhost:8080/ArtWeb/exhibition.html?"
										+ exhibition[i].idExhibition
										+ "\"'>Info</button></td></tr>";
							}
						}
						tableContent = tableContent
								+ "<tr><button type='button' onClick='addNewExhibsToMemb()'>Save</button></tr>";
						tableBody.innerHTML = tableContent;
					});
}
getExhibitionsToAddMember();

function addNewExhibsToMemb() {
	var checkedOrNot = document.getElementsByTagName("input");
	for (var i = 0; i < checkedOrNot.length; i++) {
		if (checkedOrNot[i].checked) {
			$.ajax({
				url : "http://localhost:8080/ArtWeb/rest/exhibitions/members/"
						+ id + "/exhibitions/" + checkedOrNot[i].value,
				type : "POST",
				contentType : "application/json; charset=utf-8",
				success : function() {
					location.reload();
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					console.log(textStatus);
				}
			});
		}
	}
}