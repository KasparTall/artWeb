var sessId = sessionStorage.getItem('sessionId');
console.log("session id=" + sessId);

var allMember;
function getAllMember() {
	$
			.getJSON(
					"http://localhost:8080/ArtWeb/rest/members/",
					function(member) {
						allMember = member;
						console.log(member);
						var tableBody = document
								.getElementById("membersOnFrontpage");
						var tableContent = "";
						for (var i = 0; i < member.length; i++) {
							
							tableContent = tableContent
									+ "<tr><td>"
									+ checkIfNullOrUndefined(member[i].firstName)
									+ "</td><td>"
									+ checkIfNullOrUndefined(member[i].lastName)
									+ "</td><td>"
									+ "<button type='button' onClick='parent.location=\"http://localhost:8080/ArtWeb/member.html?membId="
									+ member[i].idMembers
									+ "\"'>Profile</button></td></tr>";
						}
						tableBody.innerHTML = tableContent;
					});
}
getAllMember();

function checkIfNullOrUndefined(value) {
	if (value != null && value != undefined && value != "null") {
		return value;
	} else {
		return "";
	}
}

var allExhibitions;
function getAllExhibitions() {
	$
			.getJSON(
					"http://localhost:8080/ArtWeb/rest/exhibitions/",
					function(exhibition) {
						allExhibitions = exhibition;
						console.log(exhibition);
						var tableBody = document
								.getElementById("exhibitionsOnFrontpage");
						var tableContent = "";
						for (var i = 0; i < exhibition.length; i++) {
							tableContent = tableContent
									+ "<tr><td>"

									+ checkIfNullOrUndefined(exhibition[i].title)
									+ "</td><td>"
									+ checkIfNullOrUndefined(exhibition[i].eventDate
											.substr(0,
													allExhibitions[i].eventDate
															.lastIndexOf("T")))
									+ "</td><td>"
									+ "<button type='button' onClick='parent.location=\"http://localhost:8080/ArtWeb/exhibition.html?"
									+ exhibition[i].idExhibition
									+ "\"'>Info</button></td></tr>";
						}
						tableBody.innerHTML = tableContent;
					});
}
getAllExhibitions();

function addNewExhibitions() {
	var newExhibitionsJson = {

		"title" : document.getElementById("title").value,
		"description" : document.getElementById("description").value,
		"location" : document.getElementById("location").value,
		"eventDate" : document.getElementById("eventDate").value
				+ "T00:00:00+00:00",
		"ageRestriction" : document.getElementById("ageRestriction").value
	}
	console.log(newExhibitionsJson);
	var addExhibitions = JSON.stringify(newExhibitionsJson);
	console.log(addExhibitions);
	$.ajax({
		url : "http://localhost:8080/ArtWeb/rest/exhibitions/",
		type : "POST",
		data : addExhibitions,
		contentType : "application/json; charset=utf-8",
		success : function() {
			getAllExhibitions();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
		}
	});
}

function deleteExhibition(exId) {

	var deleteExhibitionJson = {
		"idExhibition" : exId
	};
	$.ajax({
		url : "http://localhost:8080/ArtWeb/rest/exhibitions/",
		type : "DELETE",
		data : JSON.stringify(deleteExhibitionJson),
		contentType : "application/json; charset=utf-8",
		success : function() {
			getAllExhibitions();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
		}
	});
}

function fillModifyForm(exId) {
	for (var i = 0; i < allExhibitions.length; i++) {
		if (allExhibitions[i].idExhibition == exId) {

			document.getElementById("idExhibitionModify").value = exId;
			document.getElementById("titleModify").value = allExhibitions[i].title;
			document.getElementById("descriptionModify").value = allExhibitions[i].description;
			document.getElementById("locationModify").value = allExhibitions[i].location;
			document.getElementById("eventDateModify").value = allExhibitions[i].eventDate
					.substr(0, allExhibitions[i].eventDate.lastIndexOf("T"));
			document.getElementById("ageRestrictionModify").value = allExhibitions[i].ageRestriction;
		}
	}
}

function changeExhibitions() {
	var modifyExhibitionsJson = {
		"idExhibition" : document.getElementById("idExhibitionModify").value,
		"title" : document.getElementById("titleModify").value,
		"description" : document.getElementById("descriptionModify").value,
		"location" : document.getElementById("locationModify").value,
		"eventDate" : document.getElementById("eventDateModify").value
				+ "T00:00:00+00:00",
		"ageRestriction" : document.getElementById("ageRestrictionModify").value

	}
	console.log(modifyExhibitionsJson);
	var modifyExhibitions = JSON.stringify(modifyExhibitionsJson);

	$.ajax({
		url : "http://localhost:8080/ArtWeb/rest/exhibitions/",
		type : "PUT",
		data : modifyExhibitions,
		contentType : "application/json; charset=utf-8",
		success : function() {
			getAllExhibitions();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
		}
	});
}
