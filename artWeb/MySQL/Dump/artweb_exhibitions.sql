CREATE DATABASE  IF NOT EXISTS `artweb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `artweb`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: artweb
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `exhibitions`
--

DROP TABLE IF EXISTS `exhibitions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exhibitions` (
  `idExhibition` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL,
  `description` varchar(500) NOT NULL,
  `location` varchar(45) NOT NULL,
  `eventDate` date NOT NULL,
  `ageRestriction` int(3) NOT NULL,
  PRIMARY KEY (`idExhibition`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exhibitions`
--

LOCK TABLES `exhibitions` WRITE;
/*!40000 ALTER TABLE `exhibitions` DISABLE KEYS */;
INSERT INTO `exhibitions` VALUES (1,'Roses are Blue','Paintings','Tallinn, Raekoda','2017-12-12',30),(2,'Roses are White','Graphic design','Kotka maja','2018-10-10',18),(3,'Tulips are Green','Drawings','Tallinn, Kusntihoone','2018-09-09',40),(5,'Roses are Cyan','Paintings','Tallinn, Kumu','2017-12-12',80),(14,'Cats','Sculptures','Finland, Oulu','2018-11-07',11),(16,'Horses','Sculptures','Finland, Kunno','2018-11-09',11),(17,'Cows','Sculptures','Finland, Halli','2018-11-10',11),(18,'Mice','Sculptures','Finland, Karkkila','2018-11-11',11),(19,'Rats','Sculptures','Finland, Karkkila','2018-11-12',11),(20,'Fishes','Graphics','Lake Ulemiste','2018-03-01',10),(21,'Fishes from the Past','Graphics','Underground','2018-03-01',10),(22,'Dog Leather Coats','Exclusive','Tallinn, New st','2017-01-12',12),(23,'Meadows are Interesting','Short walk','Park of Kadriorg','2018-02-24',10),(24,'Rucksacks','Micelaneous','Tartu, Kaubamaja','2018-02-24',5);
/*!40000 ALTER TABLE `exhibitions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-01 12:52:52
