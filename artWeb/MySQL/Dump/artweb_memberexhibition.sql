CREATE DATABASE  IF NOT EXISTS `artweb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `artweb`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: artweb
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `memberexhibition`
--

DROP TABLE IF EXISTS `memberexhibition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `memberexhibition` (
  `idMemb` int(5) NOT NULL,
  `idExhib` int(5) NOT NULL,
  PRIMARY KEY (`idMemb`,`idExhib`),
  KEY `idMember` (`idMemb`,`idExhib`),
  KEY `idMember_2` (`idMemb`),
  KEY `FK_membEx_exhib_idx` (`idExhib`),
  CONSTRAINT `FK_membEx_exhib` FOREIGN KEY (`idExhib`) REFERENCES `exhibitions` (`idExhibition`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_membEx_member` FOREIGN KEY (`idMemb`) REFERENCES `all_member` (`idMember`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `memberexhibition`
--

LOCK TABLES `memberexhibition` WRITE;
/*!40000 ALTER TABLE `memberexhibition` DISABLE KEYS */;
INSERT INTO `memberexhibition` VALUES (1,1),(1,2),(1,3),(4,1),(4,18),(10,1),(10,2),(10,3),(10,22),(11,2),(11,3),(11,14),(11,17),(11,18),(11,19),(11,21),(11,22),(11,24),(13,1),(13,2),(13,5),(13,17),(13,19),(13,21),(13,22),(17,3),(17,5),(17,14),(17,16),(17,18),(17,20),(17,21),(17,22),(17,23),(17,24),(18,2),(18,3),(18,16),(18,19),(18,21),(18,22),(18,24),(19,1),(19,3),(19,5),(19,14),(19,16),(19,21),(19,22),(20,1),(20,3),(20,5),(20,14),(20,16),(20,18),(20,19),(20,22),(21,1),(21,5),(21,21),(21,23),(23,21),(24,14),(24,16),(25,3),(25,14),(25,24),(37,24),(38,23),(39,5),(39,23),(40,1),(40,3),(40,5),(40,17),(40,18),(40,19),(40,20),(40,22),(40,23),(40,24),(41,3),(41,14),(41,20),(41,21),(41,22),(41,23),(44,19),(44,22),(46,20),(46,21),(47,5),(47,16),(47,18),(47,19);
/*!40000 ALTER TABLE `memberexhibition` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-01 12:52:52
